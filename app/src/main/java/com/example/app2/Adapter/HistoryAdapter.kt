package com.example.app2.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app2.R


class HistoryAdapter(val context: Context, private val resultList:List<String>):
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>(){
    class HistoryViewHolder(private val view: View):
        RecyclerView.ViewHolder(view){
      val result : TextView = view.findViewById(R.id.resultHistory)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val adapterLayout = LayoutInflater.from(
            parent.context
        ).inflate(R.layout.hist_result,parent,false)
        return HistoryViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.result.text = "RESULT:" + resultList[position]
    }

    override fun getItemCount() = resultList.size
}
