package com.example.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.app2.databinding.FragmentContinueBinding


class ContinueFragment : Fragment() {
    private var _binding: FragmentContinueBinding? = null
    private val binding get() = _binding!!
    private lateinit var holdresult: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        holdresult = arguments?.getString(HOLDR).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContinueBinding.inflate(inflater, container, false)
        val view = binding?.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnPlus?.setOnClickListener {
            val action = ContinueFragmentDirections.actionContinueFragmentToPlusFragment(conr = holdresult.toString()) //เอาเส้น
            view.findNavController().navigate(action)
        }
        binding.btnMinus.setOnClickListener {
            val action = ContinueFragmentDirections.actionContinueFragmentToMinusFragment(conr = holdresult.toString())
            view.findNavController().navigate(action)
        }
        binding?.btnMultiply?.setOnClickListener {
            val action = ContinueFragmentDirections.actionContinueFragmentToMultiplyFragment(conr = holdresult.toString()) //เอาเส้น
            view.findNavController().navigate(action)
        }
        binding?.btnDivide?.setOnClickListener {
            val action = ContinueFragmentDirections.actionContinueFragmentToDivideFragment(conr = holdresult.toString()) //เอาเส้น
            view.findNavController().navigate(action)
        }
    }

    companion object {
        const val HOLDR = "holdresult"
    }
}