package com.example.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.app2.databinding.FragmentMinusBinding


class MinusFragment : Fragment() {
    private var _binding: FragmentMinusBinding? = null
    private val binding get() = _binding!!
    private lateinit var conr: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        conr = arguments?.getString(CONR).toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMinusBinding.inflate(inflater, container, false)

        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.firstNum.setText(conr)
        binding.btnEqual.setOnClickListener {
            var num1 = binding.firstNum.text.toString().toDouble()
            var num2 = binding.secondNum.text.toString().toDouble()
            var result = num1-num2
            val action = MinusFragmentDirections.actionMinusFragmentToAnswerFragment(result = result.toString())
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val CONR = "conr"
    }
}