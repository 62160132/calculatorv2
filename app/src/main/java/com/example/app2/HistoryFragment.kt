package com.example.app2


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app2.Adapter.HistoryAdapter
import com.example.app2.Data.DataResult
import com.example.app2.databinding.FragmentHistoryBinding


class HistoryFragment : Fragment() {
    private var _binding: FragmentHistoryBinding? = null
    private val binding get() = _binding
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHistoryBinding.inflate(inflater,container,false)
        return binding?.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = DataResult.results
        recyclerView = binding?.histList!!
        recyclerView.adapter = HistoryAdapter(requireContext(),data)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding?.btnGocal?.setOnClickListener{
            val action = HistoryFragmentDirections.actionHistoryFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnDelete?.setOnClickListener{
            DataResult.Clearresult()
            onViewCreated(view, savedInstanceState)
        }
    }

    companion object {
    }
}
